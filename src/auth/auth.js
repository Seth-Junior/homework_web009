class Auth {
    constructor() {
        this.isAuth = false
    }

    login(cb) {
        this.isAuth = true
        setTimeout(() => {
            cb()
        }, 1000)
    }

    logout(cb) {
        setTimeout(() => {
            this.isAuth = false
            cb()
        }, 1000)
    }
}

export default new Auth()