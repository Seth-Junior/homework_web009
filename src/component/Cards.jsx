import React from 'react'
import {Card, Button } from 'react-bootstrap'

function Cards({data}) {
    return ( 
        <div>
            <Card className="my-2">
                <Card.Img variant="top" src={data.image} />
                <Card.Body>
                    <Card.Title>{data.title}</Card.Title>
                    <Card.Text>
                        {data.content}
                    </Card.Text>
                    <Button variant="light" size="sm" onClick={()=>alert(`Detail : ${data.id}`)}>Read</Button>
                </Card.Body>
            </Card>
        </div>
    )
}

export default Cards
