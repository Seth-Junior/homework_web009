import React from 'react'
import { Button } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useParams,
    NavLink
} from "react-router-dom";

function Animation() {
    return (
        <Router>
            <div>
                <h2>Animation Category</h2>        
                    <Button as={NavLink} variant="secondary" to="/action">Action</Button>
                    <Button as={NavLink} variant="secondary" to="/remance">Remance</Button>
                    <Button as={NavLink} variant="secondary" to="/comedy">Comedy</Button>
                <Switch>
                    <Route path="/:cat" children={<Child />} />
                </Switch>
            </div>
        </Router>
  );
}

function Child() {

  let { cat } = useParams();

  return (
    <div>
      <h4 className="mt-4"> Please Choose Category :  <span style={{color:'red'}}>{cat}</span></h4>
    </div>
  );
}



export default Animation
