import { Button } from 'react-bootstrap'
import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink
  } from "react-router-dom";
import Anima from './Animation'
import Movie from './Movie'


function Video() {
    return (
        <div>
            <h1>Video</h1>
            <Router> 
                    <Button variant="secondary" as={NavLink} to="/movie">Movie</Button>
                    <Button variant="secondary" as={NavLink} to="/animation">Animation</Button>
                    <Switch>
                        <Route path="/movie"><Movie /></Route>
                        <Route path="/animation"><Anima /></Route>
                    </Switch>
            </Router>
        </div>
    )
}

export default Video
