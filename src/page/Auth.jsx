// import React from 'react'
// import { useHistory } from 'react-router'
// import auth from '../auth/auth'

// function Auth() {

//     let history = useHistory()

//     return (
//         <div style={{width:'500px',margin:'auto'}}>
//             <h2>Login</h2>
            // <form>
            //     <div class="mb-3">
            //         <label for="exampleInputEmail1" class="form-label">Email address</label>
            //         <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
            //         <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
            //     </div>
            //     <div class="mb-3">
            //         <label for="exampleInputPassword1" class="form-label">Password</label>
            //         <input type="password" class="form-control" id="exampleInputPassword1" />
            //     </div>
            //     <div class="mb-3 form-check">
            //         <input type="checkbox" class="form-check-input" id="exampleCheck1" />
            //         <label class="form-check-label" for="exampleCheck1">Check me out</label>
            //     </div>
            // </form>
//                 <button type="submit" class="btn btn-primary" 
//                     onClick={() => {
//                         auth.login(() => {
//                             history.push('/welcome')
//                         })
//                     }}
//                 >Login</button>
//         </div>
//     )
// }

// export default Auth

import React from 'react'
import { Container, Button } from 'react-bootstrap'
import { useHistory } from 'react-router'
import auth from '../auth/auth'

function Auth() {

    let history = useHistory()

    return (
        <Container>
            <div style={{width:'500px',margin:'auto'}}>
                <h1>Login</h1>

                <form>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" />
                    </div>
                    <div class="mb-3 form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                </form>
                <Button
                    onClick={() => {
                        auth.login(() => {
                            history.push('/welcome')
                        })
                    }}
                >Login</Button>
            </div>
        </Container>
    )
}

export default Auth

