import React from 'react'
import { Col , Row } from 'react-bootstrap';
import Cards from '../component/Cards'

function Home({data}) {
    return (
        <Row>
            {data.map((data)=>{
                return(
                    <Col md={3}>
                        <Cards data={data} />
                    </Col>
                )
            })}
        </Row>
    )
}

export default Home;
